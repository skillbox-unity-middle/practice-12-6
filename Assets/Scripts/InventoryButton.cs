using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryButton : MonoBehaviour
{
    [SerializeField] private GameObject _inventory;

    public void OpenInventory()
    {
        _inventory.SetActive(true);
    }
    public void CloseInventory()
    {
        _inventory.SetActive(false);
    }
}
